//
//  AppDelegate.h
//  ByteDanceAdapter
//
//  Created by jay Win on 2019/3/15.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BUAdSDK/BUAdSDK.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,BUSplashAdDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

