//
//  BUDMopub_RewardedVideoCustomEvent.m
//  mopub_adaptor
//
//  Created by bytedance_yuanhuan on 2018/9/18.
//  Copyright © 2018年 Siwant. All rights reserved.
//

#import "BUDMopub_RewardedVideoCustomEvent.h"

#import <BUAdSDK/BUAdSDK.h>

@interface BUDMopub_RewardedVideoCustomEvent ()<BURewardedVideoAdDelegate>
@property (nonatomic, strong) BURewardedVideoAd *rewardVideoAd;

@end

@implementation BUDMopub_RewardedVideoCustomEvent

-(void)initializeSdkWithParameters:(NSDictionary *)parameters {

}
- (void)requestRewardedVideoWithCustomEventInfo:(NSDictionary *)info adMarkup:(NSString *)adMarkup {
    NSString *spotID = @"";
    if (info && [info isKindOfClass:NSDictionary.class] && info.count) {
        NSString *receivedSpotID = info[@"slotID"];
        if (receivedSpotID && [receivedSpotID isKindOfClass:NSString.class] && receivedSpotID.length) {
            spotID = receivedSpotID;
        }
    }
    if(_rewardVideoAd==nil){
        _rewardVideoAd = [[BURewardedVideoAd alloc] initWithSlotID:spotID rewardedVideoModel:[[BURewardedVideoModel alloc] init]];
        _rewardVideoAd.delegate = self;
    }
     [_rewardVideoAd loadAdData];
}

-(BOOL)hasAdAvailable {
    return _rewardVideoAd.isAdValid;
}
-(void)presentRewardedVideoFromViewController:(UIViewController *)viewController {
    [self.rewardVideoAd showAdFromRootViewController:viewController];
}
-(BOOL)enableAutomaticImpressionAndClickTracking {
    return YES;
}


#pragma mark - 回调
//加载成功
- (void)rewardedVideoAdDidLoad:(BURewardedVideoAd *)rewardedVideoAd{
    [self.delegate rewardedVideoDidLoadAdForCustomEvent:self];
}
//加载失败
- (void)rewardedVideoAd:(BURewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error{
    [self.delegate rewardedVideoDidFailToLoadAdForCustomEvent:self error:error];
}
//即将打开
- (void)rewardedVideoAdWillVisible:(BURewardedVideoAd *)rewardedVideoAd{
    [self.delegate rewardedVideoWillAppearForCustomEvent:self];
}
//已经打开
- (void)rewardedVideoAdDidVisible:(BURewardedVideoAd *)rewardedVideoAd{
    [self.delegate rewardedVideoDidAppearForCustomEvent:self];
}
//点击
- (void)rewardedVideoAdDidClick:(BURewardedVideoAd *)rewardedVideoAd{
    [self.delegate rewardedVideoDidReceiveTapEventForCustomEvent:self];
}
//即将关闭
- (void)rewardedVideoAdWillClose:(BURewardedVideoAd *)rewardedVideoAd{
    [self.delegate rewardedVideoWillDisappearForCustomEvent:self];
}
//已经关闭
- (void)rewardedVideoAdDidClose:(BURewardedVideoAd *)rewardedVideoAd{
    [self.delegate rewardedVideoDidDisappearForCustomEvent:self];
}
//发送奖励
- (void)rewardedVideoAdServerRewardDidSucceed:(BURewardedVideoAd *)rewardedVideoAd verify:(BOOL)verify{
            MPRewardedVideoReward *reward = [[MPRewardedVideoReward alloc] initWithCurrencyType:kMPRewardedVideoRewardCurrencyTypeUnspecified amount:@(kMPRewardedVideoRewardCurrencyAmountUnspecified)];
     [self.delegate rewardedVideoShouldRewardUserForCustomEvent:self reward:reward];
}

@end
