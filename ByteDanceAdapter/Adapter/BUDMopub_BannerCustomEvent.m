//
//  BUDMopub_BannerCustomEvent.m
//  BUAdSDKDemo
//
//  Created by bytedance_yuanhuan on 2018/10/24.
//  Copyright © 2018年 bytedance. All rights reserved.
//

#import "BUDMopub_BannerCustomEvent.h"
#import <BUAdSDK/BUNativeExpressBannerView.h>
#import <BUAdSDK/BUSize.h>

#define iPadDevice (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@interface BUDMopub_BannerCustomEvent ()<BUNativeExpressBannerViewDelegate>
@property(nonatomic, strong) BUNativeExpressBannerView *bannerView;
@end

@implementation BUDMopub_BannerCustomEvent
- (void)requestAdWithSize:(CGSize)size customEventInfo:(NSDictionary *)info adMarkup:(NSString *)adMarkup {
    NSString *spotID = @"";
    if (info && [info isKindOfClass:NSDictionary.class] && info.count) {
        NSString *receivedSpotID = info[@"slotID"];
        if (receivedSpotID && [receivedSpotID isKindOfClass:NSString.class] && receivedSpotID.length) {
            spotID = receivedSpotID;
        }
    }
    if (self.bannerView == nil) {
        CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
        CGFloat bannerHeigh = screenWidth/600*90;
        BUSize *imgSize = [BUSize sizeBy:BUProposalSize_Banner600_150];
        self.bannerView = [[BUNativeExpressBannerView alloc] initWithSlotID:spotID rootViewController:[UIViewController new] imgSize:imgSize adSize:CGSizeMake(screenWidth, bannerHeigh) IsSupportDeepLink:YES];
        self.bannerView.frame = CGRectMake(0, -5, screenWidth, bannerHeigh);
        self.bannerView.delegate = self;
    }
    [self.bannerView loadAdData];
}
- (BOOL)enableAutomaticImpressionAndClickTracking{
    return  YES;
}
#pragma mark -  回调
//加载成功
- (void)nativeExpressBannerAdViewDidLoad:(BUNativeExpressBannerView *)bannerAdView{
    [self.delegate bannerCustomEvent:self didLoadAd:self.bannerView];

}
//加载失败
- (void)nativeExpressBannerAdView:(BUNativeExpressBannerView *)bannerAdView didLoadFailWithError:(NSError *)error{
    [self.delegate bannerCustomEvent:self didFailToLoadAdWithError:error];

  
}
//点击
- (void)nativeExpressBannerAdViewDidClick:(BUNativeExpressBannerView *)bannerAdView{
    [self.delegate bannerCustomEventDidFinishAction:self];
    [self.delegate trackClick];
}
//渲染失败
- (void)nativeExpressBannerAdViewRenderFail:(BUNativeExpressBannerView *)bannerAdView error:(NSError *)error{
    [self.delegate bannerCustomEvent:self didFailToLoadAdWithError:error];
}
//展示
- (void)nativeExpressBannerAdViewWillBecomVisible:(BUNativeExpressBannerView *)bannerAdView{
  
}
//渲染成功
- (void)nativeExpressBannerAdViewRenderSuccess:(BUNativeExpressBannerView *)bannerAdView{

}
//不喜欢
- (void)nativeExpressBannerAdView:(BUNativeExpressBannerView *)bannerAdView dislikeWithReason:(NSArray<BUDislikeWords *> *)filterwords{
    
}
@end

